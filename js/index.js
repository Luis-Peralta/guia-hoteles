// el carousel interval es para aumentar o disminur la velocidad de movimiento

$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    // tipos de eventos de jquery con el modal: 
    $('#contacto').on('show.bs.modal', function (e){
        console.log('el modal se esta mostrando');

        $('#contactoBtn').removeClass('btn-outline-warning');
        $('#contactoBtn3').removeClass('btn-outline-warning');
        $('#contactoBtn2').removeClass('btn-outline-warning');
        // esto cambia de estilo el boton al abrir el modal
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn3').addClass('btn-primary');
        $('#contactoBtn2').addClass('btn-primary');
        // esto deshabilita el boton
        $('#contactoBtn').prop('disabled', true);
        $('#contactoBtn3').prop('disabled', true);
        $('#contactoBtn2').prop('disabled', true);

    });

    $('#contacto').on('shown.bs.modal', function (e){
        console.log('el modal se mostro');
    });

    $('#contacto').on('hide.bs.modal', function (e){
        console.log('el modal contacto se oculta');
    });

    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('el modal se oculto');
        // cuando el usuario cierra el modal se vuelve a habilitar
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn3').prop('disabled', false);
        $('#contactoBtn2').prop('disabled', false);
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn3').removeClass('btn-primary');
        $('#contactoBtn2').removeClass('btn-primary');
        // esto cambia de estilo el boton al abrir el modal
        $('#contactoBtn').addClass('btn-outline-warning');
        $('#contactoBtn3').addClass('btn-outline-warning');
        $('#contactoBtn2').addClass('btn-outline-warning');
    });


});